xfonts-kappa20 (1:0.396-7) unstable; urgency=medium

  * Trim trailing whitespace.
  * debian/control
    - Set Standards-Version: 4.5.1
    - Set Build-Depends: debhelper-compat (= 13)
    - Add Rules-Requires-Root: no
    - Move Section: fonts from x11
  * debian/copyright
    - Fix field name typo (﻿Format => Format).
    - Drop tab

 -- Hideki Yamane <henrich@debian.org>  Wed, 30 Jun 2021 22:24:26 +0900

xfonts-kappa20 (1:0.396-6.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Sun, 03 Jan 2021 13:57:40 +0100

xfonts-kappa20 (1:0.396-6) unstable; urgency=medium

  * debian/rules
    - avoid build failure with directory nonexistent

 -- Hideki Yamane <henrich@debian.org>  Sat, 10 Feb 2018 15:43:45 +0900

xfonts-kappa20 (1:0.396-5) unstable; urgency=medium

  * debian/control
    - move Vcs-* to salsa.debian.org
    - use debhelper (>= 11)
    - set Standards-Version: 4.1.3
  * debian/compat
    - set 11
  * debian/copyright
    - update copyright year and use https

 -- Hideki Yamane <henrich@debian.org>  Sat, 10 Feb 2018 15:16:30 +0900

xfonts-kappa20 (1:0.396-4) unstable; urgency=medium

  * Just bump up debian revision since dak treat 1:0.396-3 as same as 0.396-3
    and cannot upload to repository
  * debian/control
    - set Standards-Version: 4.1.0

 -- Hideki Yamane <henrich@debian.org>  Tue, 29 Aug 2017 19:45:01 +0900

xfonts-kappa20 (1:0.396-3) unstable; urgency=medium

  * debian/control
    - set Standards-Version: 4.0.0
    - set Build-Depends: debhelper (>= 10)
    - drop Suggests: xfs, removed from repostiroy
      see http://bugs.debian.org/733958
  * debian/compat
    - set 10
  * debian/rules
    - specify --no-parallel option since upstream Makefile doesn't consider
      parallel build
  * update debian/patches/Makefile.patch

 -- Hideki Yamane <henrich@debian.org>  Tue, 27 Jun 2017 21:20:45 +0900

xfonts-kappa20 (1:0.396-2) unstable; urgency=medium

  * debian/patches
    - don't ship /usr/share/fonts/X11/misc/fonts.alias (Closes: #808306)

 -- Hideki Yamane <henrich@debian.org>  Mon, 21 Dec 2015 23:06:32 +0900

xfonts-kappa20 (1:0.396-1) unstable; urgency=medium

  * repack upstream tarball
  * debian/control
    - set maintainer as Debian Fonts Task Force with previous maintainer's ack.
    - set Uploader as me.
    - set Standards-Version: 3.9.6
    - add Multi-Arch: foreign
    - use Breaks: instead of Conflicts:
  * debian/compat
    - set 9, fix "package-uses-deprecated-debhelper-compat-version"
  * debian/source/format
    - specify "3.0 (quilt)"
  * debian/install
    - move target list from debian/rules
  * debian/rules
    - simplfy, use dh, fix "should be rebuilt with current dh_installxfonts"
      (Closes: #682052)
      It also fix "debian-rules-missing-recommended-target" and
      "dh-clean-k-is-deprecated" lintian warning
    - use -n option for gzip to avoid "package-contains-timestamped-gzip"
  * debian/copyright
    - convert to copyright format 1.0
      fix "debian-copyright-file-uses-obsolete-national-encoding"

 -- Hideki Yamane <henrich@debian.org>  Thu, 17 Dec 2015 21:17:48 +0900

xfonts-kappa20 (0.396-3) unstable; urgency=low

  * Update Standards-Version.
  * debian/control: debhelper is added into Build-Depends instead of
    Build-Depends-Indep.

 -- GOTO Masanori <gotom@debian.org>  Sun,  7 May 2006 10:29:14 +0900

xfonts-kappa20 (0.396-2.1) unstable; urgency=low

  * NMU (acknowledged by gotom on IRC)
  * adapt new X fonts directory structure, closes: #362386
     - fonts are moved to /usr/share/fonts/X11/misc
     - xfonts-ayu.alias is installed into /etc/X11/fonts/X11R7/misc
     - update version of Build-Depends-Indep: debhelper to handle
       these directory structure

 -- ISHIKAWA Mutsumi <ishikawa@debian.org>  Wed, 26 Apr 2006 01:55:23 +0900

xfonts-kappa20 (0.396-2) unstable; urgency=low

  * debian/control: Change maintainer address.
  * debian/control: Update Description.
  * debian/README.Debian: Removed.
  * debian/copyright: Fix translatation typo.

 -- GOTO Masanori <gotom@debian.org>  Sun, 13 Mar 2005 11:11:11 +0900

xfonts-kappa20 (0.396-1) unstable; urgency=low

  * new upstream release.
  * debian/conffiles: removed.
  * debian/docs: removed.
  * now put upstream tar ball, not extract debian dir on
    its source dir.

 -- GOTO Masanori <gotom@debian.or.jp>  Tue, 30 Dec 2003 16:59:22 +0900

xfonts-kappa20 (0.395-1) unstable; urgency=low

  * new upstream release.
  * Some ISO-8859-1 fonts are fixed and added.  (Closes: #203186)

 -- GOTO Masanori <gotom@debian.or.jp>  Tue, 29 Jul 2003 10:17:35 +0900

xfonts-kappa20 (0.394-4) unstable; urgency=low

  * debian/control: change build-depends to build-depends-indep.
  * debian/control: add depends xutils (>> 4.0.3)
  * debian/control: add conflicts xbase-clients (<< 4.0).
  * debian/rules: comment out dh_testversion.
  * debian/conffiles: added.

 -- GOTO Masanori <gotom@debian.or.jp>  Sun, 18 Nov 2001 23:19:54 +0900

xfonts-kappa20 (0.394-3) unstable; urgency=low

  * debian/control: fixed architecture from any to all. (closes: Bug#93833)

 -- GOTO Masanori <gotom@debian.or.jp>  Sat, 14 Apr 2001 13:18:04 +0900

xfonts-kappa20 (0.394-2) unstable; urgency=low

  * debian/control: fixed typo. (closes: Bug#93719)

 -- GOTO Masanori <gotom@debian.or.jp>  Thu, 12 Apr 2001 19:24:58 +0900

xfonts-kappa20 (0.394-1) unstable; urgency=low

  * Initial Release.

 -- GOTO Masanori <gotom@debian.or.jp>  Mon, 19 Mar 2001 03:10:40 +0900
